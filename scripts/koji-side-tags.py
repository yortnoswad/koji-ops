#! /usr/bin/python

"""Side tags."""

from __future__ import print_function

from datetime import datetime
from optparse import OptionParser


import koji


def _add_dur(dur, ret, nummod, suffix, static=False):
    mod = dur % nummod
    dur = dur // nummod
    if mod > 0 or (static and dur > 0):
        ret.append(suffix)
        if static and dur > 0:
            ret.append("%0*d" % (len(str(nummod)), mod))
        else:
            ret.append(str(mod))
    return dur
def format_duration(seconds, static=False):
    if seconds is None:
        seconds = 0
    dur = int(seconds)

    ret = []
    dur = _add_dur(dur, ret, 60, "s", static=static)
    dur = _add_dur(dur, ret, 60, "m", static=static)
    ret = []
    dur = _add_dur(dur, ret, 24, "h", static=static)
    dur = _add_dur(dur, ret,  7, "d", static=static)
    if dur > 26:
        return "over 6m"
    if dur > 0:
        ret.append("w")
        ret.append(str(dur))
    return "".join(reversed(ret))

def _dt_dur(dt1, dt2):
    dt1 = dt1.timestamp()
    dt2 = dt2.timestamp()
    return format_duration(dt1 - dt2)

def _dt_duro(dt1, dt2):
    diff = dt1 - dt2
    if diff.days > 7*13:
        return 'over 3 months ago'
    elif diff.days == 1:
        return '1 day ago'
    elif diff.days > 1:
        return '{} days ago'.format(diff.days)
    elif s <= 300:
        return 'just now'
    else:
        return '{} minutes ago'.format(diff.minutes)

def dt_dur(dt1, dt2):
    return "(%8s ago)" % _dt_dur(dt1, dt2)

def main():
    parser = OptionParser()
    parser.add_option("", "--koji-host", dest="koji_host",
                      help="Host to connect to", default="https://kojihub.stream.centos.org/kojihub")
    parser.add_option("", "--tag",
                      help="Koji base tag to limit", default=None)


    (options, args) = parser.parse_args()

    kapi = koji.ClientSession(options.koji_host)

    st = kapi.listSideTags(basetag=options.tag)
    st = sorted(st, key=lambda x: (x['name'], x['user_name']))
    now = datetime.now()
    for i in st:
        # print("JDBG:", kapi.getTag(i['name']))
        nodes = kapi.getFullInheritance(i['id'])
        name = i['name']
        baseid = ''
        for node in nodes:
            if node['child_id'] == i['id']:
                if baseid != '':
                    baseid += ', '
                baseid += node['name']
        print("%-30s %-30s %s" % (name, baseid, i['user_name']))
        binfos = kapi.listTagged(i['id'])
        first = last = None
        for binfo in binfos:
            if first is None:
                first = binfo
                last = binfo
                continue
            fcs = datetime.fromisoformat(first['creation_time'])
            lcs = datetime.fromisoformat(last['creation_time'])
            cs = datetime.fromisoformat(binfo['creation_time'])
            if cs < fcs:
                first = binfo
            if cs > lcs:
                last = binfo

        if first is None:
            print("", "* No tagged packages found")
        else:
            bi = first
            tm = datetime.fromisoformat(bi['creation_time'])
            tms = tm.strftime("%Y-%m-%d %H:%M")

            print("\\_", tms, dt_dur(now, tm), bi['owner_name'], bi['nvr'])

            bi = last
            tm = datetime.fromisoformat(bi['creation_time'])
            tms = tm.strftime("%Y-%m-%d %H:%M")

            print("\\_", tms, dt_dur(now, tm), bi['owner_name'], bi['nvr'])

# Badly written but working python script
if __name__ == "__main__":
    main()

